'''
file name : bitbucket_service
description : integration and mlops services from BitBucket
started date : 20/03/2022

released date :        
released version :   major.minor.release     
changes : user flows 
changes date :    

main author : faaraan   
co_author :  kyaw 

This demo or service is based on TruEra
'''

# *Check Installed Platforms SDK Version*
# *Check Installed Python Platforms Package Version*
# *If not found the necessary pakage version then installed here*

# TruEra Internal API calling package
# pip install truera-4.0.1-py3-none-any.whl

# Basic Libraries
import os
from unittest import result
from grpc import Status
import pandas as pd
import datetime
import numpy as np

import requests
from requests.auth import HTTPBasicAuth

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import pickle

# TruEra Libraries
from truera.client.truera_workspace import TrueraWorkspace
from truera.client.truera_authentication import BasicAuthentication

class TruEra_Service():

    # Define Global Variables -> Will replace with self pointer
    global auth
    global tru
    global segment1
    global segment2
    global male
    global explainer

    # Will combine subfunction of User Authentication, Add Data and Create Model and Generate Reports as two APIs

    def __init__(self,user_auth_dict):
        self.CONNECTION_STRING = user_auth_dict['url']
        self.USERNAME = user_auth_dict['user_name']
        self.PASSWORD = user_auth_dict['password']
        

    # User Authentication 
    def set_user_authentication(self):
        status = ""       
      
        auth = BasicAuthentication(self.USERNAME, self.PASSWORD)
        global tru 
        tru= TrueraWorkspace(self.CONNECTION_STRING, auth)

        try:
            status = "1" #"Successfully login"
        except:
            status = "0" #"Login Failed"

        return status

    # Add Project, Data and Model
    def add_project_data_and_model(self, project_model_dict):
        status = ""

        tru.set_environment("remote")   # go to initilaization 
        tru.add_project(project_model_dict['project_name'], score_type="logits")
        
        tru.add_data_collection(project_model_dict['data_collection_name'])

        # X = X
        # Y = Y
        # X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.4, random_state=0)
        # model = GradientBoostingClassifier(n_estimators=50, max_depth=3, subsample=0.7)
        # model.fit(X_train, y_train)
        # model.score(X_test, y_test)

        tru.add_data_split(project_model_dict['in-sample'], project_model_dict['X'], label_data=project_model_dict['Y'], split_type="all")
        
        if project_model_dict["pickle"]:
            with open(project_model_dict["model_name"], 'rb') as f:
                model = pickle.load(f)

        tru.add_python_model(project_model_dict["model_name"], project_model_dict["model"])
        
        try:
            status = "1" # "Successfull"
        except:
            status = "0" #"Process Failed"

        return status

    # Generating Fainrness Result 
    def get_fairness_analysis(self, project_model_dict):

        tru.set_environment("remote") # go to initilaization 
        tru.set_project(project_model_dict['project_name'])
        tru.set_model(project_model_dict['model_name'])
        
        # explainer = tru.get_explainer(base_data_split="in-sample")
        self.explainer = tru.get_explainer(base_data_split=project_model_dict['base_data_split_name'])
        self.explainer.compute_fairness(
        segment_group=project_model_dict['segment_group_name'],
        segment1=project_model_dict['segment1'],
        segment2=project_model_dict['segment2'],
        fairness_type="DISPARATE_IMPACT_RATIO",
        threshold=0.5,
        threshold_score_type="probits")
        segment1 = project_model_dict['segment1']
        segment2 = project_model_dict['segment2']
        segment_group = tru.get_segment_groups()[project_model_dict['segment_group_name']]
        segments = segment_group.get_segments()

        # Multiple Dynmic Segments Later ?
        male_explainer = tru.get_explainer(base_data_split=project_model_dict['base_data_split_name'])
        male_explainer.set_segment(segments[segment1])
        female_explainer = tru.get_explainer(base_data_split=project_model_dict['base_data_split_name'])
        female_explainer.set_segment(segments[segment2])
        
        # male segment
        male = male_explainer.get_xs().describe(include="all").T
        
        # female segment
        global female
        female = female_explainer.get_xs().describe(include="all").T

        status = 1 # Done without error
        # status = 0 # Done without error

        return status, [male, female]

    # Generating Fainrness Graph 
    def get_fairness_graphs(self, column_name):
        global fainess_graphs_plot
        fainess_graphs_plot = self.explainer.plot_isp(column_name) # need to check right instance pass here 

        status = 1 # Done without error
        # status = 0 # Done without error
        
        return status, fainess_graphs_plot

    # Generating PDF Report 
    def get_pdf_report(self, report_dict):
        Status = "    "
        fig, ax =plt.subplots(figsize=(12,4))
        fontdict={
        'verticalalignment': 'baseline'}
        plt.rcParams["figure.figsize"] = [7.00, 3.50]
        plt.rcParams["figure.autolayout"] = True

        ax.set_title(report_dict['title_name_1'])

        ax.axis('tight')
        ax.axis('off')
        # male =male 
        male = pd.DataFrame(report_dict['in_arr_1']).reset_index()
        the_table = ax.table(cellText=male.values,colLabels=male.columns,cellLoc ='center', 
            loc ='upper left')
        the_table.auto_set_font_size(False)
        the_table.set_fontsize(4)

        fig1, ax =plt.subplots(figsize=(12,4))
        ax.set_title(report_dict['title_name_2'])
        ax.axis('tight')
        ax.axis('off')
        female = pd.DataFrame(report_dict['in_arr_2']).reset_index()
        the_table = ax.table(cellText=female.values,colLabels=female.columns,cellLoc ='center', 
            loc ='upper left')
        the_table.auto_set_font_size(False)
        the_table.set_fontsize(4)

        # fig2, ax =plt.subplots(figsize=(12,4))
        # ax.axis('tight')
        # ax.axis('off')
        # female = pd.DataFrame(female)
        # the_table = ax.table(cellText=female.values,colLabels=female.columns,loc='center')

        #https://stackoverflow.com/questions/4042192/reduce-left-and-right-margins-in-matplotlib-plot
        pp = PdfPages(report_dict['save_file_name'])
        pp.savefig(fig, bbox_inches='tight')
        pp.savefig(fig1, bbox_inches='tight')

        pp.close()
        
        status = "PDF Succesfully Created"
        # status = "PDF not Created"
        
        return status
        
    # Generating Stability Result 
    def get_stability_analysis(self, project_model_dict):
        arr = []
        
        global explainer_stability
        
        tru.set_environment(project_model_dict['set_environment'])
        tru.set_project(project_model_dict['project_name'])
        tru.set_model(project_model_dict['model_name'])
        
        self.explainer_stability = tru.get_explainer(base_data_split=project_model_dict['base_data_split'], comparison_data_splits=["test"])
        arr.append(self.explainer_stability.compute_performance("RMSE"))
        
        self.instability = self.explainer_stability.compute_feature_contributors_to_instability("regression")
        arr.append(self.instability.T.sort_values(by="test", ascending=False))

        status  =  1 # No Error
        #status  =  0 # with Error
        return status, arr

    # Generating Stability Graph   
    def get_stability_graph(self, columns_name,base_data_split):
        # self.explainer_stability = tru.get_explainer(base_data_split=project_model_dict['base_data_split'])
        self.explainer_stability = tru.get_explainer(base_data_split=base_data_split)

        status  =  1 # No Error
        #status  =  0 # with Error
        return status, self.explainer_stability.plot_isp(columns_name)

# Here could be SCB desired or wishing approach
    # Potential and expecting APIs from TruEra 
    # # Importing the Models
    # def import_model(user_auth_dict, project_data_dict, project_model_dict, column_name,report_dict):
    #     # model_status = ""
    #     reports_status = ""

    #     usr_auth_status = set_user_authentication(user_auth_dict)
    #     if usr_auth_status == "1":
    #         add_prj_data_mdl_status = add_project_data_and_model(project_data_dict)
    #         # Validating the Model Type Accepted by the TruEra here
    #         if add_prj_data_mdl_status == "1":
    #             reports_status, result_pdf = get_reports(project_model_dict, report_dict)   
    #         else:
    #             print("Retry for User Add Project Data Model")

    #     else:
    #         print("Retry for User Authentication")

    #     return reports_status, result_pdf

    
    # def get_reports(project_model_dict, report_dict):
    #     reports_status = ""
    #     fairness_status = ""
    #     stability_status = ""
    #     result_pdf = {}

    #     fairness_analysis_status, [male, female] = get_fairness_analysis(project_model_dict)
    #     # Later may be all published reports will consolidate as one PDF
    #     if fairness_analysis_status == "1":
    #         fairness_graphs_status, fainess_graphs_plot = get_fairness_graphs(column_name)
    #         if fairness_graphs_status == "1":
    #             pdf_report_status = get_pdf_report(report_dict)
    #             # result_pdf['fairness_report'] = get_pdf_report(report_dict)

    #             if pdf_report_status == "1":
    #                 fairness_status = "1"
    #                 reports_status = "1"
    #                 print("Explainability(Fairness) Report Are Ready to Download or Upload or Attach to the Governance Approver")
    #             else:
    #                 fairness_status = "0"
    #                 reports_status = "0"
    #                 print("Retry for Get PDF Report")
    #         else:
    #                     print("Retry for Get Fairness Graphs")
    #     else:
    #         print("Retry for Get Fairness Analysis")  

    #     stability_analysis_status, stability_arr = get_stability_analysis(project_model_dict)
    #     if stability_analysis_status == "1":
    #         stabilit_graphs_status, stability_graphs_plot = get_stability_graph(column_name,project_model_dict)
    #         if fairness_graphs_status == "1":
    #             pdf_report_status = get_pdf_report(report_dict)
    #             # result_pdf['stability_report'] = get_pdf_report(report_dict)

    #             if pdf_report_status == "1":
    #                 stability_status = "1"
    #                 reports_status = "1"
    #                 print("Explainability(Stability) Report Are Ready to Download or Upload or Attach to the Governance Approver")
    #             else:
    #                 stability_status = "0"
    #                 reports_status = "0"
    #                 print("Retry for Get PDF Report")
    #         else:
    #             print("Retry for Get Stability Graphs")
    #     else:
    #         print("Retry for Get Stability Analysis")  

    #     return reports_status, result_pdf

# # To implement Code with below Pseudos following possible and potential user flow
# # Below will demo with Jupyter
# def scb_main():
#     # Set Parameters
#     # set_user_auth_dict parameters 
#     # url, USERNAME, PASSWORD
#     user_auth_dict = ""
#     project_data_dict = ""
#     project_model_dict = ""
#     column_name = ""
#     report_dict = {}
    
#     reports_status, result_pdf =   import_model(user_auth_dict, project_data_dict, project_model_dict, column_name,report_dict)

# Pls use below this
# CG means our proposed or planned approach
# This for unit testing inside the whole class when there were graphical display
def cg_main():
    user_auth_dict = ""
    project_data_dict = ""
    project_model_dict = ""
    report_dict = ""

    te_svr = TruEra_Service()
    reports_status = ""

    usr_auth_status = te_svr.set_user_authentication(user_auth_dict)
    if usr_auth_status == "1":
        add_prj_data_mdl_status = te_svr.add_project_data_and_model(project_data_dict)
        # Validating the Model Type Accepted by the TruEra here
        if add_prj_data_mdl_status == "1":
            reports_status, result_pdf = te_svr.get_reports(project_model_dict, report_dict)   
        else:
            print("Retry for User Add Project Data Model")

    else:
        print("Retry for User Authentication")

    eports_status = ""
    fairness_status = ""
    stability_status = ""
    result_pdf = {}
    column_name = ""

    fairness_analysis_status, [male, female] = te_svr.get_fairness_analysis(project_model_dict)
    # Later may be all published reports will consolidate as one PDF
    if fairness_analysis_status == "1":
        fairness_graphs_status, fainess_graphs_plot = te_svr.get_fairness_graphs(column_name)
        if fairness_graphs_status == "1":
            pdf_report_status = te_svr.get_pdf_report(report_dict)
            # result_pdf['fairness_report'] = get_pdf_report(report_dict)

            if pdf_report_status == "1":
                fairness_status = "1"
                reports_status = "1"
                print("Explainability(Fairness) Report Are Ready to Download or Upload or Attach to the Governance Approver")
            else:
                fairness_status = "0"
                reports_status = "0"
                print("Retry for Get PDF Report")
        else:
                    print("Retry for Get Fairness Graphs")
    else:
        print("Retry for Get Fairness Analysis")  

    stability_analysis_status, stability_arr = te_svr.get_stability_analysis(project_model_dict)
    if stability_analysis_status == "1":
        stabilit_graphs_status, stability_graphs_plot = te_svr.get_stability_graph(column_name,project_model_dict)
        if fairness_graphs_status == "1":
            pdf_report_status = te_svr.get_pdf_report(report_dict)
            # result_pdf['stability_report'] = get_pdf_report(report_dict)

            if pdf_report_status == "1":
                stability_status = "1"
                reports_status = "1"
                print("Explainability(Stability) Report Are Ready to Download or Upload or Attach to the Governance Approver")
            else:
                stability_status = "0"
                reports_status = "0"
                print("Retry for Get PDF Report")
        else:
            print("Retry for Get Stability Graphs")
    else:
        print("Retry for Get Stability Analysis")  

# def main():
#     print("1")

# if __name__ =="__main__":
#     main()